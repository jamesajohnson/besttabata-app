# Run certbot to generate SSL keys
certbot certonly --webroot -w /usr/share/nginx/html -d timer.besttabata.com -m james.09.1994@gmail.com --agree-tos

# Update default Nginx.conf to use new SSL conf
cat /root/letsencrypt/secure.conf > /etc/nginx/conf.d/default.conf

# Restart Nginx
/etc/init.d/nginx reload
