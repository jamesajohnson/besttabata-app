var app = new Vue({
    el: '#app',
    data: {
        activeTab: 'section--timer-run',
        // Run Card
        workout: {
            name: 'My Workout',
            intervals: [
                {
                    action: 'Prep',
                    time: 10000
                },
                {
                    action: 'Work',
                    time: 7000
                },
                {
                    action: 'Rest',
                    time: 3000
                },
                {
                    action: 'Work',
                    time: 7000
                }
            ]
        },
        timerRunning: false,
        activeInterval: {
            action: 'Prep',
            time: 10000
        },
        nextInterval: {
            action: '',
            time: ''
        },
        frontEndUpdateTimer: '',
        intervalTimer: '',

        // Create WorkCard
        showNewWorkModal: false,
        newWorkName: '',
        newWorkMinutes: '',
        newWorkSeconds: '',
        newWorkout: {
            name: 'New Workout',
            intervals: [
                {
                    action: 'Prep',
                    time: 10000
                }
            ]
        },

        // Create Rest Card
        showNewRestModal: false,
        newRestMinutes: '',
        newRestSeconds: '',

        // Create Repeat Card
        showNewRepeatModal: false,
        newRepeatIterations: '',

        // Browse Tab
        exampleWorkouts: [
        {
            name: 'Finger Board, 7s:3s x 6',
            intervals: [
                {
                    action: 'Prep',
                    time: 5000
                },
                {
                    action: 'Work',
                    time: 7000
                },
                {
                    action: 'Rest',
                    time: 3000
                },
                {
                    action: 'Repeat',
                    iterations: 6
                }
            ]
        },
        {
            name: 'Finger Board, 30s:2m x 3',
            intervals: [
                {
                    action: 'Prep',
                    time: 5000
                },
                {
                    action: 'Work',
                    time: 30000
                },
                {
                    action: 'Rest',
                    time: 120000
                },
                {
                    action: 'Repeat',
                    iterations: 3
                }
            ]
        },
        {
            name: 'HIIT 1, 30s:2m x 3',
            intervals: [
                {
                    action: 'Prep',
                    time: 5000
                },
                {
                    action: 'Sprint',
                    time: 40000
                },
                {
                    action: 'Rest',
                    time: 40000
                },
                {
                    action: 'Push Ups',
                    time: 40000
                },
                {
                    action: 'Rest',
                    time: 40000
                },
                {
                    action: 'Squats',
                    time: 40000
                },
                {
                    action: 'Rest',
                    time: 40000
                },
                {
                    action: 'Left Side Plank',
                    time: 40000
                },
                {
                    action: 'Rest',
                    time: 40000
                },
                {
                    action: 'Right Side Plank',
                    time: 40000
                },
                {
                    action: 'Rest',
                    time: 40000
                },
                {
                    action: 'Mountain Climbers',
                    time: 40000
                },
                {
                    action: 'Rest',
                    time: 40000
                },
                {
                    action: 'Burpies',
                    time: 40000
                },
                {
                    action: 'Rest',
                    time: 40000
                },
                {
                    action: 'Push Ups',
                    time: 40000
                },
                {
                    action: 'Rest',
                    time: 40000
                },
                {
                    action: 'Lunges',
                    time: 40000
                },
                {
                    action: 'Rest',
                    time: 40000
                },
                {
                    action: 'Mountain Climbers',
                    time: 40000
                }
            ]
        }]
    },
    methods: {
        setTimer: function (start) {
            if (start) {
                // Start the timer
                this.stepThroughTimerIntervals();

                this.frontEndUpdateTimer = setInterval(i => {
                    this.decrementActiveIntervalTime()
                }, 100);
            } else {
                // Reset the timer
                clearInterval(this.frontEndUpdateTimer);
                clearTimeout(this.intervalTimer);
                this.activeInterval = this.workout.intervals[0];
                this.nextInterval = this.workout.intervals[1];
                this.workout = this.newWorkout;
            }
        },
        /**
        *   Step Through the interals in the timers, waiting for the amount specified at each interval
        *   @param    start    int    The index of the interval to start the timer at
        */
        stepThroughTimerIntervals: function (start = 0) {
            var max = this.workout.intervals.length - 1;

            // Is the timer complete?
            if (start > max) {
                clearInterval(this.frontEndUpdateTimer);

                this.activeInterval = {
                    action: 'Complete',
                    time: ''
                };

                this.nextInterval = {
                    action: '',
                    time: ''
                };

                return;
            } else if (this.workout.intervals[start].action === 'Repeat' && this.workout.intervals[start].iterations > 0) {
                // Is this a repeat interval
                var repeatFromInterval = this.getIntervalToRepeatFrom(start);
                this.workout.intervals[start].iterations = this.workout.intervals[start].iterations - 1;
                this.stepThroughTimerIntervals(repeatFromInterval);
            } else {
                // Update the Interval information displayed
                this.activeInterval = {
                    action: this.workout.intervals[start].action,
                    time: this.workout.intervals[start].time
                }

                this.nextInterval = this.workout.intervals[start + 1] ? 
                    this.workout.intervals[start + 1] : {
                        action: 'Complete',
                        time: ''
                    };

                // Start the timer with this interval
                this.timeInterval(this.activeInterval.time).then(res => {
                    // Proceed to the next interval
                    this.stepThroughTimerIntervals(start + 1);
                });
            }
        },
        // Return the index of the interval to start repeating from
        // e.g. return 1, the first interval
        // or return the index of the first interval after a previous repeat
        getIntervalToRepeatFrom: function (repeatInterval) {
            repeatInterval = repeatInterval - 1;
            var i;
            for (i = repeatInterval; i >= 0; i --) {
                if (this.workout.intervals[i].action === 'Repeat' || this.workout.intervals[i].action === 'Prep') {
                    console.log("repeating from: " + (i + 1));
                    return i + 1;
                }
            }
        },
        timeInterval: function (time) {
            return new Promise(function(resolve, reject) {
                this.intervalTimer = setTimeout(function () {
                    resolve("Timer Complete");
                }, time);
            });
        },
        // Take 100ms off the running intervals time, only used on the front end
        decrementActiveIntervalTime: function() {
            this.activeInterval.time = this.activeInterval.time - 100;
        },
        // Empty the newWorkout.intervals array and and a prep interval
        clearWorkoutIntervals: function () {
            this.newWorkout.intervals = [{
                action: 'Prep',
                time: 10000
            }];
        },
        // Take the values from the New Work Modal and add them to the newWorkout.intervals array
        addWorkToWorkout: function () {
            this.showNewWorkModal = false;
            this.newWorkout.intervals.push({
                action: this.newWorkName,
                time: (this.newWorkMinutes * 60000) + (this.newWorkSeconds * 1000)
            });
        },
        // Take the values from the New Rest Modal and add them to the newWorkout.intervals array
        addRestToWorkout: function () {
            this.showNewRestModal = false;
            this.newWorkout.intervals.push({
                action: 'Rest',
                time: (this.newRestMinutes * 60000) + (this.newRestSeconds * 1000)
            });
        },
        // Add a Repeat to the workout
        addRepeatToWorkout: function () {
            this.showNewRepeatModal = false;
            this.newWorkout.intervals.push({
                action: 'Repeat',
                iterations: this.newRepeatIterations
            });
        },
        // Copy the newWorkout to the activeWorkout and run show the run tab
        runNewWorkout: function () {
            this.workout = this.newWorkout;
            this.activeTab = 'section--timer-run';
        }
    },
    filters: {
        hoursMinsSeconds: function (mills) {
            var milliseconds = parseInt((mills%1000)/100)
            var seconds = parseInt((mills/1000)%60)
            var minutes = parseInt((mills/(1000*60))%60)

            minutes = (minutes < 10) ? "0" + minutes : minutes;
            seconds = (seconds < 10) ? "0" + seconds : seconds;

            return minutes + ":" + seconds + "." + milliseconds;
        }
    },
    computed: {
        totalWorkoutTime: function () {
            var totalTime = 0;

            this.workout.intervals.forEach(interval => {
                totalTime = totalTime + interval.time;
            });

            return totalTime;
        }
    }
})
